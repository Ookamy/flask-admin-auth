"""
Initialization module for the admin_auth package in the Flask application.

This module contains the version and author information for the admin_auth 
package.

:Example:

    from App.admin_auth import __version__, __author__

Attributes:
    __version__ (str): The version of the admin_auth package.
    __author__ (str): The author of the admin_auth package.

"""

__version__ = "1.1"
__author__ = "Ookamy"
