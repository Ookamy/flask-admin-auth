"""
Module containing the routes and views for the Flask application.

This module defines the routes and views of the Flask application using the
Flask framework and Flask extensions.
"""

from flask import (
    current_app as app,
    g,
    render_template,
)
from App import login_manager

from .admin_auth import consts
from .admin_auth.model.user import User
from .admin_auth.model.admin import AppConfig


@login_manager.user_loader
def load_user(user_id):
    """
    Load and return user from the database or memory.

    Args:
        user_id (int): The ID of the user to load.

    Returns:
        User: The user object corresponding to the given ID.
    """
    return User.query.get(int(user_id))


@app.before_request
def before_request():
    """
    Add tools to the context before handling the request.

    This function sets the URLs and parameters in the global `g` object.
    """
    g.urls = consts
    g.params = AppConfig.query.get(1)


@app.context_processor
def inject_context():
    """
    Make tools available in Jinja2 templates.

    Returns:
        dict: A dictionary containing the URLs and parameters.
    """
    return {"urls": consts, "params": AppConfig.query.get(1)}


@app.route("/", methods=["GET"])
def home():
    """
    Render the home page of the application.

    Returns:
        str: Rendered HTML template for the home page.
    """
    return render_template(
        "index.html",
    )


@app.errorhandler(401)
def unauthorized_error_401(error):
    """
    Render the unauthorized error page for HTTP 401.

    Args:
        error (HTTPException): The HTTP 401 error object.

    Returns:
        tuple: A tuple containing the rendered HTML template and the HTTP status code.
    """
    return (
        render_template("errors/401.html", admin_email=consts.ADMIN_EMAIL, error=error),
        401,
    )


@app.errorhandler(403)
def unauthorized_error_403(error):
    """
    Render the unauthorized error page for HTTP 403.

    Args:
        error (HTTPException): The HTTP 403 error object.

    Returns:
        tuple: A tuple containing the rendered HTML template and the HTTP status code.
    """
    return (
        render_template("errors/403.html", admin_email=consts.ADMIN_EMAIL, error=error),
        403,
    )


@app.errorhandler(404)
def unauthorized_error_404(error):
    """
    Render the not found error page for HTTP 404.

    Args:
        error (HTTPException): The HTTP 404 error object.

    Returns:
        tuple: A tuple containing the rendered HTML template and the HTTP status code.
    """
    return render_template("errors/404.html", error=error), 404
