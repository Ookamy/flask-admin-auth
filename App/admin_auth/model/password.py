from App import *
import re

UUID = "PWD_CONFIG"


class PasswordConfig(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String(10), unique=True, nullable=False)  # STR_KEY
    minimum_characters_required = db.Column(db.Integer, default=10)
    lowercase_required = db.Column(db.Boolean, default=False)
    uppercase_required = db.Column(db.Boolean, default=False)
    digit_required = db.Column(db.Boolean, default=False)
    special_characters_required = db.Column(db.Boolean, default=False)
    special_characters = db.Column(db.String(50), default="_!@#$%^&*+\-=:;.?")

    def __init__(
        self,
        uuid="PWD_CONFIG",
        minimum_characters_required: int = 10,
        lowercase_required: bool = False,
        uppercase_required: bool = False,
        digit_required: bool = False,
        special_characters_required: bool = False,
        special_characters: str = "_!@#$%^&*+\-=:;.?",
    ):
        self.uuid = uuid
        self.minimum_characters_required = minimum_characters_required
        self.lowercase_required = lowercase_required
        self.uppercase_required = uppercase_required
        self.digit_required = digit_required
        self.special_characters_required = special_characters_required
        self.special_characters = special_characters

    def __repr__(self):
        return f"{self.uuid}, {self.minimum_characters_required}, {self.lowercase_required}, {self.uppercase_required}, {self.digit_required}, {self.special_characters_required}, {self.special_characters}"

    @classmethod
    def get_required_characters(cls):
        required_characters = ""
        if cls.lowercase_required:
            required_characters += "abcdefghijklmnopqrstuvwxyz"
        if cls.uppercase_required:
            required_characters += "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        if cls.digit_required:
            required_characters += "0123456789"
        if cls.special_characters_required:
            required_characters += cls.special_characters
        return required_characters

    @classmethod
    def validate_password_characters(cls, password):
        required_characters = cls.get_required_characters()
        required_characters_str = str(required_characters)
        escaped_required_characters = re.escape(required_characters_str)
        regex = re.compile(f"^[{escaped_required_characters}]+$")
        return regex.match(password) is not None

    @property
    def to_dict(self):
        return {
            "uuid": self.uuid,
            "minimum_characters_required": self.minimum_characters_required,
            "lowercase_required": self.lowercase_required,
            "uppercase_required": self.uppercase_required,
            "digit_required": self.digit_required,
            "special_characters_required": self.special_characters_required,
            "special_characters": self.special_characters,
        }
