import unittest

from App import create_app, db

from ..model.password import PasswordConfig


class TestPasswordConfigModel(unittest.TestCase):
    def setUp(self):
        """Method executed before each test."""
        # Crée une instance de la base de données pour les tests
        self.app = create_app("testing")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.pwdconfig = PasswordConfig()
        self.customconfig = PasswordConfig(
            uuid="CUSTOM_CFG",
            minimum_characters_required=8,
            lowercase_required=True,
            uppercase_required=True,
            digit_required=True,
            special_characters_required=True,
            special_characters="@#$",
        )
        db.session.add(self.pwdconfig)
        db.session.commit()

    def tearDown(self):
        """Method executed after each test."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_default_config(self):
        """Test initial password configuration."""
        # self.assertEqual(self.pwdconfig.uuid, "PWD_CONFIG")
        self.assertEqual(self.pwdconfig.minimum_characters_required, 10)
        self.assertFalse(self.pwdconfig.lowercase_required)
        self.assertFalse(self.pwdconfig.uppercase_required)
        self.assertFalse(self.pwdconfig.digit_required)
        self.assertFalse(self.pwdconfig.special_characters_required)
        self.assertEqual(self.pwdconfig.special_characters, "_!@#$%^&*+\-=:;.?")

    def test_custom_config(self):
        password_config = PasswordConfig(
            uuid="CUSTOM_CFG",
            minimum_characters_required=8,
            lowercase_required=True,
            uppercase_required=True,
            digit_required=True,
            special_characters_required=True,
            special_characters="@#$",
        )
        # self.assertEqual(password_config.uuid, "CUSTOM_CFG")
        self.assertEqual(password_config.minimum_characters_required, 8)
        self.assertTrue(password_config.lowercase_required)
        self.assertTrue(password_config.uppercase_required)
        self.assertTrue(password_config.digit_required)
        self.assertTrue(password_config.special_characters_required)
        self.assertEqual(password_config.special_characters, "@#$")

    def test_wrong_password(self):
        """Test la configuration par defaut"""
        password_1 = "Pass with spaces"
        password_2 = 'FlaskAp@=)+=)&é"(-è_çàp2024'
        password_3 = "abc123"
        password_4 = "SecurePass789"
        self.assertFalse(self.customconfig.validate_password_characters(password_1))
        self.assertFalse(self.customconfig.validate_password_characters(password_2))
        self.assertFalse(self.customconfig.validate_password_characters(password_3))
        self.assertFalse(self.customconfig.validate_password_characters(password_4))

    # def test_create_password_config(self):
    #     """Tests default __init__."""
    #     pwd_config = PasswordConfig()
    #     "Not None"
    #     self.assertIsNotNone(pwd_config.uuid)
    #     self.assertIsNotNone(pwd_config.minimum_characters_required)
    #     self.assertIsNotNone(pwd_config.lowercase_required)
    #     self.assertIsNotNone(pwd_config.uppercase_required)
    #     self.assertIsNotNone(pwd_config.digit_required)
    #     self.assertIsNotNone(pwd_config.special_characters_required)
    #     self.assertIsNotNone(pwd_config.special_characters)
    #     "change config"
    #     self.assertEqual(10, pwd_config.minimum_characters_required)
    #     pwd_config.minimum_characters_required = 15
    #     self.assertEqual(15, pwd_config.minimum_characters_required)
    #     "test boolean"
    #     self.assertFalse(pwd_config.lowercase_required)
    #     self.assertFalse(pwd_config.uppercase_required)
    #     self.assertFalse(pwd_config.digit_required)
    #     self.assertFalse(pwd_config.special_characters_required)
    #     pwd_config.lowercase_required = True
    #     pwd_config.uppercase_required = True
    #     pwd_config.digit_required = True
    #     pwd_config.special_characters_required = True
    #     self.assertTrue(pwd_config.lowercase_required)
    #     self.assertTrue(pwd_config.uppercase_required)
    #     self.assertTrue(pwd_config.digit_required)
    #     self.assertTrue(pwd_config.special_characters_required)

    # test -> longueur trop court, egal et +
    # test -> au moins 1 majucule
    # test -> au moins 1 minuscule
    # test -> au moins 1 chiffre
    # test -> au moins 1 caractère spécial
    # test -> au moins x caractère spécial (ajouter dans la class des mdps)
    # test -> refuser si un caractère n'est pas present dans la liste pwd_config.special_characters)
    # def test_length_is_ok(self):
    #     """Test la longueur du mdp"""

    #     pwd_config = PasswordConfig()
    #     badpwd="azerty"
    #     goodpwd="SuperP0t@t0ze"
    #     userpassword_1 = "1234"
    #     userpassword_2 = "AZERTY"
    #     print(self.pwdconfig.minimum_characters_required)
    #     # self.assertGreaterEqual(len(userpassword_1), pwd_config.minimum_characters_required)
    #     self.assertLess(len(goodpwd), pwd_config.minimum_characters_required)

    # def test_is_capitalized(self):
    #     """ """
    #     pass

    # def test_is_lowercase(self):
    #     """ """
    #     pass

    # does_have_special_character
    # does_have_number
    # does_have_capital_lettre
    # does_have_lower_case
