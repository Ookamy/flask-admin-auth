$(document).ready(function () {
  let sidebar = document.querySelector(".sidebar");
  let sidebarBtn = document.querySelector(".sidebarBtn");
  if (sidebarBtn) {
    sidebarBtn.onclick = function () {
      sidebar.classList.toggle("active");
      if (sidebar.classList.contains("active")) {
        sidebarBtn.classList.replace("fa-compress", "fa-expand");
      } else {
        sidebarBtn.classList.replace("fa-expand", "fa-compress");
      };
    };
  }

  $('form').on("keydown", function (event) {
    // block the enter key in forms
    if (event.which === 13) {
      event.preventDefault();
    }
  });

});


function falshAlert() {
  setTimeout(function () {
    $("#flash").removeClass("animate--drop-in-fade-out");
  }, 3500);
}

function validateEmail(email) {
  // Expression régulière pour la validation de l'email
  var expressionEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return expressionEmail.test(email);
}

function validatePassword(motDePasse) {
  // Définir les expressions régulières pour les différentes conditions
  var longueurMinimale = 8;
  var expressionLettreMajuscule = /[A-Z]/;
  var expressionChiffre = /[0-9]/;
  var expressionCaractereSpecial = /[_!@#$%^&*+\-=;:.?]/;
  var expressionCaracteresAutorises = /^[a-zA-Z0-9_!@#$%^&*+\-=;:.?]*$/;

  // Vérifier la longueur minimale
  if (motDePasse.length < longueurMinimale) {
    return false;
  }

  // Vérifier la présence d'une lettre majuscule
  if (!expressionLettreMajuscule.test(motDePasse)) {
    return false;
  }

  // Vérifier la présence d'un chiffre
  if (!expressionChiffre.test(motDePasse)) {
    return false;
  }

  // Vérifier la présence d'un caractère spécial
  if (!expressionCaractereSpecial.test(motDePasse)) {
    return false;
  }
  // Vérifier la présence de caractères autorisés
  if (!expressionCaracteresAutorises.test(motDePasse)) {
    return false;
  }
  // Si toutes les conditions sont satisfaites, le mot de passe est valide
  return true;
}

function ToggleView(status, status_txt, toggleId){
  let toggle = $('#'+toggleId);
  if (status == status_txt){
    toggle.prop('checked', true);
  }else{
    toggle.prop('checked', false);
  }
}