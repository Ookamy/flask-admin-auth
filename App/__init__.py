"""
Module containing the application factory and initialization code for the Flask
application.

This module initializes and configures the Flask application, sets up the
database, and registers the blueprints.

:Example:

    from App import create_app

Attributes:
    db (SQLAlchemy): SQLAlchemy object for database operations.
    login_manager (LoginManager): LoginManager object for managing user authentication.
    SIMPLE_CAPTCHA (CAPTCHA): CAPTCHA object for handling captcha functionalities.

Functions:
    create_app: Application factory to create and configure the Flask application.

"""

import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from dotenv import load_dotenv
from flask_simple_captcha import CAPTCHA
from App.settings import Config, ConfigTest


db = SQLAlchemy()
login_manager = LoginManager()
load_dotenv()

SIMPLE_CAPTCHA = CAPTCHA(
    config={
        "SECRET_CAPTCHA_KEY": os.environ.get("SECRET_CAPTCHA_KEY"),
        "EXPIRE_SECONDS": 60 * 10,
        "CAPTCHA_IMG_FORMAT": "JPEG",
        "CAPTCHA_LENGTH": 6,
        "CAPTCHA_DIGITS": True,
        "ONLY_UPPERCASE": True,
        "EXCLUDE_VISUALLY_SIMILAR": True,
        "BACKGROUND_COLOR": (53, 3, 128),
        "TEXT_COLOR": (256, 256, 256),
    }
)


def create_app(test_config=None):
    """
    Application factory to create and configure the Flask application.

    Args:
        test_config (ConfigTest, optional): The configuration for testing. Defaults to None.

    Returns:
        Flask: The initialized and configured Flask application.
    """
    app = Flask(__name__, instance_relative_config=False)
    if test_config is None:
        # Application Configuration
        app.config.from_object(Config)
    else:
        # Application Configuration for testing
        app.config.from_object(ConfigTest)

    # Initialize Plugins
    db.init_app(app)
    Migrate(app, db)
    login_manager.init_app(app)
    SIMPLE_CAPTCHA.init_app(app)

    with app.app_context():
        # Import and register blueprints
        from App import routes
        from App.admin_auth.routes import admin, auth

        app.register_blueprint(admin)
        app.register_blueprint(auth)

        # Add new module here
        # from App.NEW_MODULE import routes
        # app.register_blueprint(routes.BLUEPRINT_NAME)

        # Create Database Models
        db.create_all()

    return app
