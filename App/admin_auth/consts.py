"""
Module defining application constants and routes for authentication and admin management.

Attributes:
    ADMIN_EMAIL (str): Email address of the administrator.
    APP_NAME (str): Name of the application.

    HOME (str): Home route name.

    STATIC (str): Static files route name.
    LOGO (str): Path to the application logo.
    FAVICO (str): Path to the favicon file.
    STYLE (str): Path to the CSS stylesheet.
    JS (str): Path to the JavaScript file.

    USER_LOGGED (str): Route for user authentication status.
    USER_ACCOUNT (str): Route for user account management.
    UPDATE_PASSWORD (str): Route for updating user password.
    UPDATE_USER (str): Route for updating user details.
    REGISTER (str): Route for user registration.
    LOGIN (str): Route for user login.
    LOGOUT (str): Route for user logout.

    ADMIN_DB (str): Route for admin dashboard.
    ADMIN_SECURITY (str): Route for admin security settings.
    ADMIN_USERS (str): Route for managing users.
    ACTIVATE_USER (str): Route for activating users.
    DELETE_USER (str): Route for deleting users.
    ROLE_USER (str): Route for updating user roles.
    ADMIN_CAPTCHA (str): Route for admin captcha settings.
    ADMIN_HONEYPOT (str): Route for admin honeypot settings.
    ADMIN_REGISTRATION (str): Route for admin registration settings.
    CHECK_USERNAME (str): Route for checking username availability.
    CHECK_EMAIL (str): Route for checking email availability.

    TP_BASE (str): Base directory for templates.
    TP_FORMS (str): Directory for form templates.
    TP_DB (str): Directory for admin-related templates.
    TP_USER (str): Directory for user-related templates.
"""

ADMIN_EMAIL = "mme.ookamy@gmail.com"
APP_NAME = "IOT For Noob"
# URL_FOR #
HOME = "home"
# STATIC
STATIC = "admin.static"
LOGO = "img/OokaLogo.svg"
FAVICO = "img/favicon.ico"
STYLE = "css/style.css"
JS = "js/app.js"

# USER Authentication
USER_LOGGED = "auth.user"
USER_ACCOUNT = "auth.account"
UPDATE_PASSWORD = "auth.update_password"
UPDATE_USER = "auth.update_user"
REGISTER = "auth.register"
LOGIN = "auth.login"
LOGOUT = "auth.logout"

# ADMIN
ADMIN_DB = "admin.admin_dashboard"
ADMIN_SECURITY = "admin.admin_security"
ADMIN_USERS = "admin.admin_users"
ACTIVATE_USER = "admin.user_activate"
DELETE_USER = "admin.user_erase"
ROLE_USER = "admin.user_update_role"
ADMIN_CAPTCHA = "admin.admin_captcha"
ADMIN_HONEYPOT = "admin.admin_honeypot"
ADMIN_REGISTRATION = "admin.admin_registration"
CHECK_USERNAME = "admin.check_username"
CHECK_EMAIL = "admin.check_email"

# render_template dir
TP_BASE = "base-components/"
TP_FORMS = "forms/"
TP_DB = "admin/"
TP_USER = "user/"
