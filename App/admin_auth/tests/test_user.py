import unittest

from App import create_app, db

from ..model.user import User


class TestUserModel(unittest.TestCase):

    def setUp(self):
        """Method executed before each test."""
        # Crée une instance de la base de données pour les tests
        self.app = create_app("testing")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Method executed after each test."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_password_setter(self):
        """Tests whether the password is correctly hashed when set."""
        user = User(username="test", email="test@example.com")
        user.password = "password123"
        self.assertTrue(user.password_hash is not None)

    def test_password_verification(self):
        """Tests whether password verification works correctly."""
        user = User(username="test", email="test@example.com")
        user.password = "password123"
        self.assertTrue(user.check_password("password123"))
        self.assertFalse(user.check_password("wrongpassword"))


if __name__ == "__main__":
    unittest.main()
