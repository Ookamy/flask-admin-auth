"""
Module containing the database model for admin configurations in the Flask application.

This module defines the AppConfig class, which represents the admin
configurations stored in the database.

:Example:

    from App.admin_auth.model.admin import AppConfig

Classes:
    AppConfig (class): Database model class for admin configurations.

Attributes:
    UUID (str): Constant string representing the UUID for AppConfig.
"""

from App import db

UUID = "APP_CONFIG"


class AppConfig(db.Model):
    """
    Database model for storing admin configurations in the Flask application.

    Attributes:
        id (int): Primary key identifier for the AppConfig entry.
        uuid (str): Unique identifier for the application configuration.
        captcha_enable (bool): Flag to enable or disable captcha.
        honeypot_enable (bool): Flag to enable or disable honeypot security.
        registration_enable (bool): Flag to enable or disable user registration.
    """

    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String(10), unique=True, nullable=False)
    captcha_enable = db.Column(db.Boolean, default=False)
    honeypot_enable = db.Column(db.Boolean, default=False)
    registration_enable = db.Column(db.Boolean, default=False)

    def __init__(
        self,
        uuid: str = "APP_CONFIG",
        captcha_enable: bool = False,
        registration_enable: bool = False,
        honeypot_enable: bool = False,
    ):
        """
        Initializes an AppConfig instance with given configuration settings.

        Args:
            uuid (str): Unique identifier for the configuration. Default is "APP_CONFIG".
            captcha_enable (bool): Whether captcha is enabled. Default is False.
            registration_enable (bool): Whether user registration is enabled. Default is False.
            honeypot_enable (bool): Whether honeypot security is enabled. Default is False.
        """
        self.uuid = uuid
        self.captcha_enable = captcha_enable
        self.honeypot_enable = honeypot_enable
        self.registration_enable = registration_enable

    @property
    def captcha_active(self):
        """
        Property method to check if captcha is active.

        Returns:
            bool: True if captcha is active, False otherwise.
        """
        return self.captcha_enable

    @property
    def honeypot_active(self):
        """
        Property method to check if honeypot security is active.

        Returns:
            bool: True if honeypot is active, False otherwise.
        """
        return self.honeypot_enable

    @property
    def registration_active(self):
        """
        Property method to check if user registration is active.

        Returns:
            bool: True if registration is active, False otherwise.
        """
        return self.registration_enable

    @property
    def captcha_checked(self):
        """
        Property method to get the 'checked' status for captcha.

        Returns:
            str: "checked" if captcha is enabled, None otherwise.
        """
        return "checked" if self.captcha_enable else None
