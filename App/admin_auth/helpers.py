"""

Module containing helper functions and decorators for the admin_auth package in 
the Flask application.

This module provides a decorator for role-based access control.

:Example:

    from App.admin_auth.helpers import role_required

Decorators:
    role_required(role): Decorator to check if the current user has a certain 
    role before allowing access to a protected view.

"""

from functools import wraps
from flask import abort
from flask_login import current_user


def role_required(role):
    """
    Decorator that checks if the current user has a certain role before
    allowing access to a protected view.

    Args:
        role (int): The required role level to access the view.

    Returns:
        function: A decorator that checks the user's role before executing the
        associated function for the view.

    Note:
        The function uses current_user to get information about the currently
        authenticated user.
        current_user must be a global variable or an object with an
        is_authenticated property and a role property.

    Raises:
        401 Unauthorized: If the user is not authenticated.
        403 Forbidden: If the user does not have the required role.
    """

    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            # Check if the user is anonymous or not authenticated
            if not current_user.is_authenticated:
                return abort(401)  # Unauthorized

            # Check if the user has the required role
            if not hasattr(current_user, "has_role") or current_user.role < role:
                return abort(403)  # Forbidden

            # If the user is authenticated and has the required role, execute the function
            return func(*args, **kwargs)

        return wrapper

    return decorator
