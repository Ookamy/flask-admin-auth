import unittest

from App import create_app, db

from ..model.user import User
from ..model.admin import AppConfig

from ..tools import toggle_property


class TestTools(unittest.TestCase):

    def setUp(self):
        """Method executed before each test."""
        # Crée une instance de la base de données pour les tests
        self.app = create_app("testing")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        # Crée un utilisateur pour les tests
        self.user = User(username="TEST NAME", email="dot@dot.com", is_active=True)
        db.session.add(self.user)
        # Crée la config
        self.appconfig = AppConfig(uuid="TESTCONFIG")
        db.session.add(self.appconfig)
        db.session.commit()

    def tearDown(self):
        """Method executed after each test."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_toggle_property(self):
        """Test the property toggle"""
        initial_value = self.appconfig.captcha_enable
        toggle_property(db, AppConfig, self.appconfig.id, "captcha_enable")
        self.assertNotEqual(initial_value, self.appconfig.captcha_enable)
        self.assertTrue(self.appconfig.captcha_enable)

        initial_value = self.user.is_active
        toggle_property(db, User, self.user.id, "is_active")
        self.assertNotEqual(initial_value, self.user.is_active)
        self.assertFalse(self.user.is_active)

    def test_toggle_invalid_property_name(self):
        """Test behavior when property name is invalid"""
        with self.assertRaises(ValueError):
            toggle_property(db, User, self.user.id, "invalid_property")

    def test_toggle_nonexistent_object(self):
        """Test behavior when object does not exist"""
        with self.assertRaises(ValueError):
            toggle_property(db, User, 9999, "is_active")


if __name__ == "__main__":
    unittest.main()
