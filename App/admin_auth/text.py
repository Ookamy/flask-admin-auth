"""

Module containing text constants for the admin_auth package in the Flask 
application.

This module provides various text constants used throughout the admin_auth 
package.

:Example:

    from App.admin_auth.text import TXT_LOGIN_SUCCESS

Text Constants:
    TXT_LOGIN_SUCCESS (str): Message displayed on successful login.
    TXT_LOGIN_INVALID (str): Message displayed on invalid email or password during login.
    TXT_WRONG_CAPTCHA (str): Message displayed when the CAPTCHA is entered incorrectly.
    TXT_REGISTER_SUCCESS (str): Message displayed on successful user 
    registration. Uses string formatting with the username.
    TXT_REGISTER_INVALID (str): Message displayed on invalid registration form.
    TXT_REGISTER_EXIST (str): Message displayed when the user already exists 
    during registration.
    TXT_UPDATE_PROFIL_SUCCESS (str): Message displayed when the user profile is 
    successfully updated.
    TXT_UPDATE_PWD_SUCCESS (str): Message displayed when the user password is successfully updated.
    TXT_UPDATE_PWD_INVALID (str): Message displayed when the old password 
    provided is incorrect during password update.
    TXT_DELETE_ACCOUNT (str): Message displayed when the user account is successfully deleted.

"""

# to use variables, you need to add curly braces {} to the string.

TXT_LOGIN_SUCCESS = "Logged in successfully."
TXT_LOGIN_INVALID = "Invalid email or password."
TXT_WRONG_CAPTCHA = "Wrong Captcha"
TXT_REGISTER_SUCCESS = "User {} create with success!"
TXT_REGISTER_INVALID = "Invalid form"
TXT_REGISTER_EXIST = "The user already exists"

TXT_UPDATE_PROFIL_SUCCESS = (
    "Votre profile est mis à jour =) "  # "Your profile has been updated!"
)
TXT_UPDATE_PWD_SUCCESS = "Your Password has been updated!"
TXT_UPDATE_PWD_INVALID = "Incorrect old password. Please try again."

TXT_DELETE_ACCOUNT = "You account are delete with success"
