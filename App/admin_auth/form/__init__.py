"""
Module containing form definitions for the admin authentication in the Flask
application.

This module provides form classes for handling user authentication and admin
settings.

:Example:

    from App.admin_auth.form import LoginForm, SignupForm, UpdateEmailNameForm,
    UpdatePasswordForm, DeleteUserForm

Classes:
    LoginForm (class): Form class for user login.
    SignupForm (class): Form class for user registration.
    UpdateEmailNameForm (class): Form class for updating user email and name.
    UpdatePasswordForm (class): Form class for updating user password.
    DeleteUserForm (class): Form class for user deletion.

"""

from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, SubmitField, BooleanField, HiddenField
from wtforms.validators import (
    DataRequired,
    Email,
    EqualTo,
    Length,
    Optional,
    InputRequired,
    Regexp,
)
from ..security.base_form import BaseForm


# Auth User Login Create Update Delete
class LoginForm(BaseForm):
    """
    User Log-in Form.

    Attributes:
        email (StringField): The email field for the form.
        password (PasswordField): The password field for the form.
        submit (SubmitField): The submit button for the form.
    """

    email = StringField(
        "Email", validators=[DataRequired(), Email(message="Entrer un email valide.")]
    )
    password = PasswordField("Mots de passe", validators=[DataRequired()])
    submit = SubmitField("Connexion")


class SignupForm(BaseForm):
    """
    Form for creating a User.

    Attributes:
        username (StringField): The username field for the form.
        password (PasswordField): The password field for the form.
        email (StringField): The email field for the form.
        submit (SubmitField): The submit button for the form.
    """

    username = StringField(
        "Alias, nom utilisateur",
        validators=[
            DataRequired(),
            Length(min=2, message="Votre identifiant utilisateur"),
        ],
        render_kw={
            "onkeyup": "checkUsernameValid()"
        },  # Ajoutez ici votre fonction JavaScript
    )
    password = PasswordField(
        "Mot de passe",
        validators=[
            DataRequired(),
            Length(min=12, message="Créer un mot de pass fort"),
            Regexp(
                # regex="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[_!@#$%^&*+\-=:;.?])[A-Za-z\d_!@#$%^&*+\-=:;.?]+$",
                regex="^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[_!@#$%^&*+\\-=:;.?])[A-Za-z\\d_!@#$%^&*+\\-=:;.?]+$",
                message="Le mot de passe doit contenir au moins une minuscule, une majuscule, un chiffre, et un caractère spécial parmi '!@#$%^&*+-=;:.?'",
            ),
        ],
    )
    email = StringField(
        "Email",
        validators=[
            Length(min=10),
            Email(message="Entrer un email valide"),
            DataRequired(),
        ],
    )
    submit = SubmitField("Inscription")


class UpdateEmailNameForm(FlaskForm):
    """
    Form for updating User information.

    Attributes:
        username (StringField): The username field for the form.
        email (StringField): The email field for the form.
        submit (SubmitField): The submit button for the form.
    """

    username = StringField("Alias, nom utilisateur", [DataRequired()])

    email = StringField(
        "Email",
        validators=[
            Length(min=6),
            Email(message="Entrer un email valide"),
            DataRequired(),
        ],
    )
    submit = SubmitField("Update user")


class UpdatePasswordForm(FlaskForm):
    """
    Form for updating User password.

    Attributes:
        old_password (PasswordField): The old password field for the form.
        new_password (PasswordField): The new password field for the form.
        confirm_password (PasswordField): The confirm new password field for the form.
        submit (SubmitField): The submit button for the form.
    """

    old_password = PasswordField("Old Password", validators=[DataRequired()])
    new_password = PasswordField(
        "New Password", validators=[DataRequired(), Length(min=8)]
    )
    confirm_password = PasswordField(
        "Confirm New Password", validators=[DataRequired(), EqualTo("new_password")]
    )
    submit = SubmitField("Update Password")


class DeleteUserForm(FlaskForm):
    """
    Form for user deletion.

    Attributes:
        userid (HiddenField): The hidden user ID field for the form.
        username (HiddenField): The hidden username field for the form.
        confirmation_phrase (StringField): The confirmation phrase field for the form.
        confirm (HiddenField): The hidden confirm field for the form.
        submit (SubmitField): The submit button for the form.
    """

    userid = HiddenField("UserID")
    username = HiddenField("UserName")
    confirmation_phrase = StringField(
        "Confirmation Phrase",
        validators=[
            InputRequired(),
            EqualTo("confirm", message="La phrase de confirmation est incorrecte"),
        ],
    )
    confirm = HiddenField("")
    submit = SubmitField("Supprimer")
