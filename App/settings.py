"""
Module containing the configuration of the Flask application.

This module defines the configurations of the Flask application using environment variables.

:Example:

    from App.settings import Config

Attributes:
    Config (class): Main configuration class for the Flask application.
    ConfigTest (class): Configuration class for the Flask application tests.
"""

from os import environ, path
from dotenv import load_dotenv

basedir = path.abspath(path.dirname(__file__))
load_dotenv()


class Config:
    """
    Set Flask configuration from environment variables.

    Attributes:
        FLASK_APP (str): The name of the Flask application.
        SECRET_KEY (str): Secret key to secure the application.
        STATIC_FOLDER (str): The directory for static files.
        TEMPLATES_FOLDER (str): The directory for HTML templates.
        SQLALCHEMY_DATABASE_URI (str): URI of the main database.
        SQLALCHEMY_ECHO (bool): Enable or disable the SQL query output.
        SQLALCHEMY_TRACK_MODIFICATIONS (bool): Enable or disable tracking of database modifications.
    """

    # Général Config
    FLASK_APP = environ.get("FLASK_APP")
    SECRET_KEY = environ.get("FLASK_SK")
    # Static Assets
    STATIC_FOLDER = environ.get("STATIC_FOLDER")
    TEMPLATES_FOLDER = environ.get("TEMPLATES_FOLDER")
    # database
    SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI")
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ConfigTest:
    """
    Set Flask configuration from environment variables for testing.

    Attributes:
        FLASK_APP (str): The name of the Flask application for testing.
        SECRET_KEY (str): Secret key to secure the application.
        SQLALCHEMY_DATABASE_URI (str): URI of the database for testing.
        SQLALCHEMY_ECHO (bool): Enable or disable the SQL query output.
        SQLALCHEMY_TRACK_MODIFICATIONS (bool): Enable or disable tracking of database modifications.
    """

    # Général Config
    FLASK_APP = environ.get("FLASK_APP")
    SECRET_KEY = environ.get("FLASK_SK")
    # database
    SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI_TEST")
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
