"""
Main module to run the Flask application.

This module initializes and runs the Flask application using the `create_app` function from the `App` module.

:Example:

    $ flask run

Attributes:
    app (Flask): The instance of the Flask application.

"""

from App import create_app

app = create_app()

if __name__ == "__main__":
    app.run()
