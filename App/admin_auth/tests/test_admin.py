import unittest

from App import create_app, db

from ..model.admin import AppConfig


class TestAppConfigModel(unittest.TestCase):
    def setUp(self):
        """Method executed before each test."""
        # Crée une instance de la base de données pour les tests
        self.app = create_app("testing")
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Method executed after each test."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_uuid_not_null(self):
        """Tests whether the uuid field is not null."""
        app_config = AppConfig(uuid="abc123", captcha_enable=True)
        self.assertIsNotNone(app_config.uuid)

    def test_captcha_enable_default(self):
        """Tests whether the default value of captcha_enable is False."""
        app_config = AppConfig(uuid="abc123")
        self.assertFalse(app_config.captcha_enable)

    def test_captcha_enable_return_checked(self):
        """Tests the return of the captcha_checked property."""
        initial_value = "checked"
        app_config = AppConfig(uuid="abc123", captcha_enable=True)
        self.assertEqual(initial_value, app_config.captcha_checked)
        app_config.captcha_enable = not app_config.captcha_enable
        self.assertNotEqual(initial_value, app_config.captcha_checked)


if __name__ == "__main__":
    unittest.main()
