from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime

from App import db


# class Role(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     name = db.Column(db.String(80), unique=True)
#     description = db.Column(db.String(255))

#     def __str__(self):
#         return self.name


class User(UserMixin, db.Model):
    """
    User model that inherits from UserMixin for Flask-Login functionality.
    This model represents a user in the system and includes methods
    related to authentication, password management, and role-based access control.

    Attributes:
        id (int): Primary key for the user.
        username (str): The username of the user.
        email (str): The email address of the user.
        password_hash (str): Hashed password of the user.
        role (int): The role identifier for the user (default is 1).
        date_created (datetime): Timestamp of when the user account was created.
        last_login (datetime): Timestamp of the user's last login.
        is_active (bool): Boolean flag indicating whether the account is active.

    Not in database:
        ROLE (dict): Dictionary mapping role IDs to role names.
    """

    # table columns
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(120), nullable=False, unique=True)
    password_hash = db.Column(db.String(128))
    role = db.Column(db.Integer, default=1)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    last_login = db.Column(db.DateTime, onupdate=datetime.utcnow)
    is_active = db.Column(db.Boolean, nullable=False, default=True)

    # not in db
    ROLE = {
        0: "anonymous",
        1: "visitor",
        2: "moderator",
        3: "administrator",
        4: "super-administrator",
    }

    def __init__(
        self,
        username: str = "",
        email: str = "",
        password: str = "",
        role: int = 1,
        is_active: bool = True,
    ):
        """
        Initialize a new User object.

        Args:
            username (str, optional): The username of the user. Defaults to "".
            email (str, optional): The email address of the user. Defaults to "".
            password (str, optional): The password of the user. Defaults to "".
            role (int, optional): The role of the user. Defaults to 1.
            is_active (bool, optional): Boolean indicating whether the user account is active. Defaults to True.
        """
        self.username = username
        self.email = email
        self.password_hash = generate_password_hash(password, salt_length=10)
        self.role = role
        self.is_active = is_active
        self.date_created = datetime.now()

    @property
    def role_name(self):
        """
        Property method to retrieve the role name corresponding to the user's role ID.

        Returns:
            str: The name of the role corresponding to the user's role ID.
        """
        return self.ROLE.get(self.role, "Unknown")

    def has_role(self, role_key):
        """
        Checks if the user has a specific role.

        Args:
            role_key (int): The role key to check.

        Returns:
            bool: True if the user has the specified role, otherwise False.
        """
        for key, value in self.ROLE.items():
            if key == role_key:
                return self.role == key
        return False

    def min_role_required(self, role_key):
        """
        Checks if the user's role is equal to or higher than the required minimum role.

        Args:
            role_key (int): The required minimum role key.

        Returns:
            bool: True if the user's role is equal to or higher than the required role, otherwise False.
        """
        for key, value in self.ROLE.items():
            if key == role_key:
                return self.role >= key
        return False

    @property
    def full_power(self):
        """
        Property method to check if the user has full administrative rights (super-administrator).

        Returns:
            bool: True if the user is a super-administrator (role 4), otherwise False.
        """
        return self.role == 4

    @property
    def password(self):
        """
        Getter for the password attribute (raises an error as the password should not be accessed directly).

        Raises:
            AttributeError: Password cannot be accessed directly.
        """
        raise AttributeError("The password is unreadable!")

    @password.setter
    def password(self, password):
        """
        Setter for the password attribute. It hashes the password before storing it.

        Args:
            password (str): The password to set.
        """
        self.password_hash = generate_password_hash(password, salt_length=10)

    def check_password(self, password):
        """
        Checks if the provided password matches the stored password hash.

        Args:
            password (str): The password to verify.

        Returns:
            bool: True if the password matches the stored hash, otherwise False.
        """
        return check_password_hash(self.password_hash, password)

    @property
    def formatted_date_created(self):
        """
        Property method to return the account creation date formatted as 'DD/MM/YYYY'.

        Returns:
            str: The formatted creation date, or an empty string if not available.
        """
        return self.date_created.strftime("%d/%m/%Y") if self.date_created else ""

    @property
    def formatted_last_login(self):
        """
        Property method to return the last login date formatted as 'HH:MM DD/MM/YYYY'.

        Returns:
            str: The formatted last login date, or an empty string if not available.
        """
        return self.last_login.strftime("%H:%M %d/%m/%Y") if self.last_login else ""

    @property
    def is_js_active(self):
        """
        Property method to return the user's active status as an integer (1 for active, 0 for inactive).

        Returns:
            int: 1 if the user is active, 0 if inactive.
        """
        if self.is_active:
            return 1
        return 0

    def __repr__(self):
        """
        String representation of the User object (username).

        Returns:
            str: The username of the user.
        """
        return f"{self.username}"

    def __json__(self):
        """
        Custom method to return a dictionary representation of the User object for JSON serialization.

        Returns:
            dict: A dictionary with user details such as id, username, email, etc.
        """
        return {
            "id": self.id,
            "username": self.username,
            "email": self.email,
            "formatted_date_created": self.formatted_date_created,
            "formatted_last_login": self.formatted_last_login,
            "role_name": self.role_name,
            "is_active": self.is_active,
        }
