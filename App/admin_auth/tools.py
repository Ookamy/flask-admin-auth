import json
from flask import jsonify
from sqlalchemy import func
from .model.user import User
from .model.admin import AppConfig


def toggle_property(db, model, object_id: int, property_name: str):
    """
    Toggles the value of a boolean property for a specific object in the database.

    Args:
        db (SQLAlchemy): Database session object.
        model (SQLAlchemy Model): The model class to update.
        object_id (int): ID of the object to update.
        property_name (str): The name of the boolean property to toggle.

    Raises:
        ValueError: If the property name is invalid or if the object is not found.
        Exception: If any error occurs during the database operation.
    """
    try:
        if not hasattr(model, property_name):
            raise ValueError("Invalid property name.")

        obj = db.session.get(model, object_id)
        if not obj:
            raise ValueError("Object not found.")

        current_value = getattr(obj, property_name)
        setattr(obj, property_name, not current_value)

        db.session.add(obj)
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        raise e


def captcha_enabled_disabled(db, value: int = None):
    """
    Toggles the 'captcha_enable' property of the AppConfig model.

    Args:
        db (SQLAlchemy): Database session object.
        value (int, optional): The value to update the captcha setting.

    Returns:
        jsonify: A response with success or error message.
    """
    if value:
        try:
            toggle_property(db, AppConfig, 1, "captcha_enable")
            return jsonify({"message": "is_active updated successfully"}), 200
        except ValueError as ve:
            return jsonify({"message": str(ve)}), 404
        except Exception as e:
            return (
                jsonify(
                    {"message": "An error occurred while updating captcha setting."}
                ),
                500,
            )


def honeypot_enabled_disabled(db, value: int = None):
    """
    Toggles the 'honeypot_enable' property of the AppConfig model.

    Args:
        db (SQLAlchemy): Database session object.
        value (int, optional): The value to update the honeypot setting.

    Returns:
        jsonify: A response with success or error message.
    """
    if value:
        try:
            toggle_property(db, AppConfig, 1, "honeypot_enable")
            return jsonify({"message": "is_active updated successfully"}), 200
        except ValueError as ve:
            return jsonify({"message": str(ve)}), 404
        except Exception as e:
            return (
                jsonify(
                    {"message": "An error occurred while updating register setting."}
                ),
                500,
            )


def registration_enabled_disabled(db, value: int = None):
    """
    Toggles the 'registration_enable' property of the AppConfig model.

    Args:
        db (SQLAlchemy): Database session object.
        value (int, optional): The value to update the registration setting.

    Returns:
        jsonify: A response with success or error message.
    """
    if value:
        try:
            toggle_property(db, AppConfig, 1, "registration_enable")
            return jsonify({"message": "is_active updated successfully"}), 200
        except ValueError as ve:
            return jsonify({"message": str(ve)}), 404
        except Exception as e:
            return (
                jsonify(
                    {"message": "An error occurred while updating register setting."}
                ),
                500,
            )


def user_activate_deactivate(db, user_id: int = None):
    """
    Toggles the 'is_active' property for a user in the database.

    Args:
        db (SQLAlchemy): Database session object.
        user_id (int, optional): ID of the user to update.

    Returns:
        jsonify: A response with success or error message.
    """
    if user_id:
        try:
            toggle_property(db, User, user_id, "is_active")
            return jsonify({"message": "is_active updated successfully"}), 200
        except ValueError as ve:
            return jsonify({"message": str(ve)}), 404
        except Exception as e:
            return (
                jsonify(
                    {"message": "An error occurred while updating captcha setting."}
                ),
                500,
            )


def user_delete(db, user_id: int = None):
    """
    Deletes a user from the database based on their ID.

    Args:
        db (SQLAlchemy): Database session object.
        user_id (int, optional): ID of the user to delete.

    Returns:
        jsonify: A response with success or error message.
    """
    if user_id:
        user = db.session.get(User, user_id)
        if user:
            db.session.delete(user)
            db.session.commit()
            return jsonify({"message": "User deleted successfully"}), 200
        else:
            return jsonify({"message": "User not found"}), 404


def user_role(db, user_id: int = None, role: int = None):
    """
    Updates the role of a user in the database.

    Args:
        db (SQLAlchemy): Database session object.
        user_id (int, optional): ID of the user whose role will be updated.
        role (int, optional): New role for the user.

    Returns:
        jsonify: A response with success or error message.
    """
    if user_id and role:
        user = db.session.get(User, user_id)
        if user:
            user.role = int(role)
            db.session.commit()
            return (
                jsonify(
                    {
                        "message": "Role updated successfully",
                        "data": user.role_name,
                    }
                ),
                200,
            )
        else:
            return jsonify({"message": "User not found"}), 404


def username_exist(db, name: str = None):
    """
    Checks if a username exists in the database.

    Args:
        db (SQLAlchemy): Database session object.
        name (str, optional): The username to check.

    Returns:
        str: 'True' if the username exists, 'False' if it does not.
    """
    if name:
        user = User.query.filter(func.lower(User.username) == func.lower(name)).first()
        if user:
            return "True"  # Username exists in the database
        else:
            return "False"  # Username does not exist
    else:
        return "False"  # If no username is provided, assume it does not exist


def validate_password(mot_de_passe, password_config):
    """
    Validates the given password against the configured requirements.

    Args:
        mot_de_passe (str): The password to validate.
        password_config (object): The password configuration object containing the rules.

    Returns:
        bool: True if the password is valid, False otherwise.
    """
    if len(mot_de_passe) < password_config.minimum_characters_required:
        return False

    if password_config.lowercase_required and not re.search(r"[a-z]", mot_de_passe):
        return False

    if password_config.uppercase_required and not re.search(r"[A-Z]", mot_de_passe):
        return False

    if password_config.digit_required and not re.search(r"[0-9]", mot_de_passe):
        return False

    if password_config.special_characters_required and not any(
        char in password_config.special_characters for char in mot_de_passe
    ):
        return False

    return True
