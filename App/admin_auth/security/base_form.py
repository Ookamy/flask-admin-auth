from flask import g
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import ValidationError, Optional
from random import choice

TROLL_URLS = [
    "https://www.nyan.cat",  # Nyan Cat
    "https://www.zombo.com",
    "https://theuselessweb.com",  # A random useless website
    "https://media1.tenor.com/m/5EsPyV_QxGAAAAAC/minion.gif",  # Minion GIF as a string
]


class BaseForm(FlaskForm):
    """
    Base class for forms with honeypot protection.

    This class should be inherited by all other forms in the application.
    """

    user_ref = StringField(
        validators=[Optional()],
        render_kw={"class": "copperfield", "autocomplete": "off"},
    )

    def __init__(self, *args, **kwargs):
        """
        Initializes the form and adds attributes `needs_redirect` and `redirect_url`.
        """
        super().__init__(*args, **kwargs)
        self.needs_redirect = False
        self.redirect_url = None

    def validate_user_ref(self, field, extra_validators=None):
        """
        Custom validation method for the honeypot field.

        If honeypot protection is active and the field contains data,
        the request is flagged as a bot attempt, and the user is redirected
        to a troll URL.

        Args:
            field (wtforms.Field): The honeypot field to validate.
            extra_validators (list, optional): Additional validators to apply before custom validation.

        Raises:
            ValidationError: If a bot is detected.
        """
        if not g.params.honeypot_active:
            return  # Do nothing if honeypot is disabled

        # WTForms applies extra_validators before custom validations
        if extra_validators is None:
            extra_validators = []

        for validator in extra_validators:
            validator(self, field)

        if field.data:
            self.needs_redirect = True
            self.redirect_url = choice(TROLL_URLS)
            # Raise an error to prevent form submission
            raise ValidationError("Bot detected!")
