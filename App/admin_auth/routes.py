"""
routes.py
This module contains the route definitions for the Flask application.

Routes:
    - User Profile and Account Management
    - Admin Dashboard and Security Settings
    - User Activation and Role Management
    - User Deletion
    - Authentication and Registration
    - User Update and Password Management
"""

from flask import (
    Blueprint,
    g,
    flash,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_login import current_user, login_user, login_required, logout_user
from datetime import datetime
from functools import wraps

from .. import login_manager, db, SIMPLE_CAPTCHA
from .helpers import role_required
from .tools import *
from .consts import *
from .text import *
from .form import *
from .model.admin import AppConfig

# Blueprint Configuration
admin = Blueprint(
    "admin",
    __name__,
    url_prefix="/chaos",
    template_folder="templates",
    static_folder="static",
)

auth = Blueprint("auth", __name__, url_prefix="/")


@auth.route("<username>/profil", methods=["GET", "POST"])
@login_required
def user(username):
    """
    Render the user profile page.

    Args:
        username (str): The username of the authenticated user.

    Returns:
        str: Rendered user profile page.
    """
    return render_template(TP_USER + "index.html", title="Profil")


@auth.route("<username>/gestion", methods=["GET", "POST"])
@login_required
def account(username):
    """
    Render the account management page.

    Args:
        username (str): The username of the authenticated user.

    Returns:
        str: Rendered account management page.
    """
    return render_template(TP_USER + "index.html", title="Administration du compte")


# Admin Dashboard
@admin.route("/dashboard", methods=["GET", "POST"])
@login_required
@role_required(3)
def admin_dashboard():
    """
    Render the admin dashboard page.

    Returns:
        str: Rendered admin dashboard page.
    """
    return render_template(TP_DB + "index.html")


@admin.route("/security", methods=["GET", "POST"])
@login_required
@role_required(4)
def admin_security():
    """
    Render the admin security settings page.

    Returns:
        str: Rendered admin security settings page.
    """
    app_config = AppConfig.query.first()
    users = User.query.all()
    return render_template(TP_DB + "index.html", title="=)")


@admin.route("/captcha_activate", methods=["PUT"])
@login_required
@role_required(3)
def admin_captcha():
    """
    Activate or deactivate CAPTCHA for user authentication.

    Returns:
        str: JSON response indicating success or failure.
    """
    return captcha_enabled_disabled(db, request.form.get("value"))


@admin.route("/honeypot_activate", methods=["PUT"])
@login_required
@role_required(3)
def admin_honeypot():
    """
    Activate or deactivate honeypot in invalid forms.

    Returns:
        str: JSON response indicating success or failure.
    """
    return honeypot_enabled_disabled(db, request.form.get("value"))


@admin.route("/registration_activate", methods=["PUT"])
@login_required
@role_required(3)
def admin_registration():
    """
    Activate or deactivate REGISTRATION forms and routes.

    Returns:
        str: JSON response indicating success or failure.
    """
    return registration_enabled_disabled(db, request.form.get("value"))


@admin.route("/users", methods=["GET", "POST"])
@login_required
@role_required(3)
def admin_users():
    """
    Render the admin users list page.

    Returns:
        str: Rendered admin users list page.
    """
    users = User.query.all()
    return render_template(TP_DB + "index.html", users=users, subtitle="Users list : ")


@admin.route("/users/active", methods=["PUT"])
@login_required
@role_required(3)
def user_activate():
    """
    Activate or deactivate a user account.

    Returns:
        str: JSON response indicating success or failure.
    """
    user_id = request.form.get("userid")
    response = user_activate_deactivate(db, user_id)
    return response


@admin.route("/users/role", methods=["PUT", "POST"])
@login_required
@role_required(4)
def user_update_role():
    """
    Update the role of a user.

    Returns:
        str: JSON response indicating success or failure.
    """
    user_id = request.form.get("userId")
    role_id = request.form.get("userRole")
    response = user_role(db, user_id, role_id)
    return response


@admin.route("/users/delete", methods=["POST"])
@login_required
def user_erase():
    """
    Delete a user account.

    Returns:
        str: JSON response indicating success or failure.
    """
    # user_id = request.form.get("userId")
    user_id = request.form.get("aleatoire_number")
    response = user_delete(db, user_id)
    if int(response[-1]) == 200:
        flash(TXT_DELETE_ACCOUNT, "message")
    return response


@auth.route("/logout")
def logout():
    """
    Log out the current user.

    Returns:
        str: Redirect to the home page.
    """
    logout_user()
    return redirect(url_for(HOME))


@auth.route(
    "/login",
    methods=["GET", "POST"],
)
def login():
    """
    Handle user login.

    Returns:
        str: Rendered login page or redirects to the user profile or admin dashboard.
    """
    # deja auth alors rentre direct
    if current_user.is_authenticated:
        # redirection user profil
        return redirect(url_for(USER_LOGGED, username=current_user.username))

    form = LoginForm()
    new_captcha_dict = SIMPLE_CAPTCHA.create()

    if request.method == "POST":
        form = LoginForm(request=request)

        if form.validate_on_submit():
            user = User.query.filter_by(email=form.email.data).first()
            if user and user.check_password(form.password.data):
                # global params
                if g.params.captcha_active:
                    # check captcha
                    c_hash = request.form.get("captcha-hash")
                    c_text = request.form.get("captcha-text")
                    if not SIMPLE_CAPTCHA.verify(c_text, c_hash):
                        flash(TXT_WRONG_CAPTCHA, "error")
                        return redirect(url_for(LOGIN))

                login_user(user)
                user.last_login = datetime.now()
                db.session.commit()
                flash(TXT_LOGIN_SUCCESS, "success")
                if user.has_role(4):
                    return redirect(url_for(ADMIN_DB))
                return redirect(url_for(USER_LOGGED, username=user.username))
            else:
                flash(TXT_LOGIN_INVALID, "error")
        else:
            if form.needs_redirect:
                return redirect(form.redirect_url)
            else:
                for field, errors in form.errors.items():
                    for error in errors:
                        flash(f"Erreur dans {field}: {error}", "danger")

                # flash(TXT_LOGIN_INVALID, "error")

    return render_template(
        TP_FORMS + "login.html", form=form, captcha=new_captcha_dict, title="Connexion"
    )


@admin.route("/check-username", methods=["PUT"])
def check_username():
    """
    Check if a username already exists.

    Returns:
        str: JSON response indicating username availability.
    """
    uname = request.form.get("username")
    return username_exist(db, uname)


@auth.route("/register", methods=["GET", "POST"])
def register():
    """
    Handle user registration.

    Returns:
        str: Rendered registration page or redirects to the user profile.
    """

    if not g.params.registration_active:
        return redirect(url_for(LOGIN))

    form = SignupForm()
    new_captcha_dict = SIMPLE_CAPTCHA.create()
    if request.method == "POST":
        form = SignupForm(request=request)
        if form.validate_on_submit():
            # global params
            if g.params.captcha_active:
                # check captcha
                c_hash = request.form.get("captcha-hash")
                c_text = request.form.get("captcha-text")
                if not SIMPLE_CAPTCHA.verify(c_text, c_hash):
                    flash(TXT_WRONG_CAPTCHA, "error")
                    return redirect(url_for(REGISTER))
            existing_user = User.query.filter_by(email=form.email.data).first()
            if existing_user is None:
                user_obj = User(
                    username=form.username.data,
                    email=form.email.data,
                    password=form.password.data,
                )
                db.session.add(user_obj)
                db.session.commit()  # Create new user
                login_user(user_obj)  # Log in as newly created user
                flash(TXT_REGISTER_SUCCESS.format(user_obj.username), "message")
                return redirect(url_for(USER_LOGGED, username=user_obj.username))
            else:
                flash(TXT_REGISTER_EXIST, "error")
        else:
            if form.needs_redirect:
                return redirect(form.redirect_url)
            else:
                for field, errors in form.errors.items():
                    for error in errors:
                        flash(f"Erreur dans {field}: {error}", "danger")

    return render_template(
        TP_FORMS + "register.html",
        form=form,
        captcha=new_captcha_dict,
        title="Inscription",
    )


@auth.route("/user", methods=["GET", "POST"])
@login_required
def update_user():
    """
    Update the username and email of the current user.

    Returns:
        str: Rendered update user email page or redirects to the user profile.
    """
    form = UpdateEmailNameForm()
    if request.method == "POST":
        form = UpdateEmailNameForm(request=request)
        if form.validate_on_submit():
            current_user.username = form.username.data
            current_user.email = form.email.data
            db.session.commit()
            flash(TXT_UPDATE_PROFIL_SUCCESS, "success")
            return redirect(url_for(USER_LOGGED, username=current_user.username))
    elif request.method == "GET":
        form.username.data = current_user.username
        form.email.data = current_user.email

    return render_template(
        TP_FORMS + "update_user_email.html",
        form=form,
        title="Update Username and Email",
    )


@auth.route("/security", methods=["GET", "POST"])
@login_required
def update_password():
    """
    Update the password of the current user.

    Returns:
        str: Rendered update password page or redirects to the user profile.
    """
    form = UpdatePasswordForm()
    if request.method == "POST":
        form = UpdatePasswordForm(request=request)
        if form.validate_on_submit():
            if current_user.check_password(form.old_password.data):
                current_user.password = form.new_password.data
                db.session.commit()
                flash(TXT_UPDATE_PWD_SUCCESS, "success")
                return redirect(url_for(USER_LOGGED, username=current_user.username))
            else:
                flash(TXT_UPDATE_PWD_INVALID, "danger")
    return render_template(
        TP_FORMS + "update_password.html", form=form, title="Update your password"
    )
