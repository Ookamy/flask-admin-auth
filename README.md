# flask-admin-auth
Version: 1.0
Author:  Ookamy, Emmanuelle GOUGELET


## Description
"Flask admin auth" a pour but de rendre le début d'un développement sous Flask plus rapide.
Ce module évite de devoir repenser à chaque fois a la partie authentification des utilisateurs.
C'est imparfait et en cours d'évolution. Ce projet est en cours d'évolution. 
Il y a encore et toujours des choses à mettre en place, tel que le captcha ou le paramétrage des mots de passe. 
Et, évidemment, mettre en place les tests unitaires.

"Flask admin auth" is designed to speed up the start of Flask development.
This module avoids having to rethink user authentication every time. It's imperfect and still evolving. 
This project is still evolving. There are still things to be done, such as captcha and password settings. 
And, of course, unit testing.

### CSS :
Actuellement, le site n'est pas réactif car il s'agit d'un CSS personnel. Si vous souhaitez remodeler et ajouter des frameworks frontaux ou adapter le CSS à votre goût, amusez-vous !

Currently, the site is not responsive, as it's a personal CSS. If you'd like to remodel and add front-end frameworks or adapt the CSS to your taste, have fun!

## Getting started
 1. Create dev env:
    > python -m venv env
    
    > source env/bin/activate
 2. Requirement
    > pip install -r requirements.txt
 3. Database init 
    > flask db init
 4. Insert first commit
    >  #soon
 5. Run Flask App
    > flask run --debug

## pip list
- pip install Flask 
- pip install flask-login 
- pip install flask-sqlalchemy
- pip install flask_wtf
- pip install python-dotenv
- pip install email_validator
- pip install Flask-Migrate
- pip install pillow


## Application
Le module "admin_auth" est indépendant du reste de l'application : il possède ses propres fichiers html dans "templates" et CSS, JavaScript, image dans "static". 

The "admin_auth" module is independent of the rest of the application: it has its own html files in "templates" and CSS, JavaScript and image files in "static". 

### Constantes
Le fichier 'admin_auth/consts.py' regroupe les URL du module si vous modifiez la fonction 'home' pensez a modifier aussi ce fichier pour ne pas être perdu lors de la déconnexion. 

The 'admin_auth/consts.py' file contains the module's URLs. If you modify the 'home' function, remember to modify this file too, so you don't get lost when you disconnect.

### Captcha
- pip install flask-simple-captcha (https://pypi.org/project/flask-simple-captcha/)

Datatable : AppConfig - value : captcha_enable (Boolean by default 0)

À l'image des constantes, la table AppConfig est injectée dans la variable g (globals) de l'application. Sous le nom 'params'. Cette fonctionnalité concerne les formulaires loggin et register. Pour modifier les couleurs texte et background, c'est dans l'initialisation de l'application __init__.py à la racine du projet.

Like constants, the AppConfig table is injected into the application's g (globals) variable. Under the name 'params'. This functionality concerns the loggin and register forms.
To modify text and background colors, go to application initialization __init__.py

### Template with

#### switch
```
# html include
{% with label="", id="", value="", is_cheched="", js_function="" %}
    {% include urls.TP_FORMS+'switch.html' %}
{% endwith %}
```





## Database tips
### Flask-Migrate 

https://flask-migrate.readthedocs.io/en/latest/
- database initialization, if used for the first time, creates a migration folder in the project
    > flask db init
- creation of a migration
    > flask db migrate -m "Initial migration."
- application of migrations, update database with migration
    > flask db upgrade

### Loaddata sqlite3
I use sqlite3 in my development environment.
- Create Dump
    > sqlite3 your_database.db .dump > database_dump.sql
- Load Dump
    > sqlite3 your_database.db < database_dump.sql
    > sqlite3 instance/project.db < database_dump.sql
Superpot@toes42

## Test 
Model Appconfig and User, tools.py file
 > python -m unittest -v

